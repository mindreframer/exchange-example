defmodule Exchange.PriceLevelsTest do
  use ExUnit.Case
  alias Exchange.PriceLevels
  alias Exchange.Event

  describe "PriceLevels" do
    test "insert/update/delete works and respects bounds" do
      pl = PriceLevels.new(:ask)

      {:ok, pl} = pl |> add_event(:new, :ask, 1, 10, 5)
      assert [{10, 5}] = PriceLevels.items(pl)

      {:ok, pl} = pl |> add_event(:new, :ask, 1, 20, 10)
      assert [{20, 10}, {10, 5}] = PriceLevels.items(pl)

      {:ok, pl} = pl |> add_event(:update, :ask, 1, 30, 10)
      assert [{30, 10}, {10, 5}] = PriceLevels.items(pl)

      {:ok, pl} = pl |> add_event(:update, :ask, 2, 50, 15)
      assert [{30, 10}, {50, 15}] = PriceLevels.items(pl)

      {:ok, pl} = pl |> add_event(:new, :ask, 2, 60, 13)
      assert [{30, 10}, {60, 13}, {50, 15}] = PriceLevels.items(pl)

      {:ok, pl} = pl |> add_event(:delete, :ask, 2, 1000, 1000)
      assert [{30, 10}, {50, 15}] = PriceLevels.items(pl)

      {:error, "outofbounds: index: 3"} = pl |> add_event(:delete, :ask, 3, 1000, 1000)
      {:error, "outofbounds: index: 3"} = pl |> add_event(:update, :ask, 3, 1000, 1000)
      # cant insert a price level at a position that has no previous position
      {:error, "outofbounds: index: 4"} = pl |> add_event(:new, :ask, 4, 60, 15)
      # errors on bad index input
      {:error, "outofbounds: index: b"} = pl |> add_event(:delete, :ask, "b", 1000, 1000)

      # errors on bad item input
      {:error, "bad item format: {\"b\", \"c\"}"} = pl |> add_event(:new, :ask, 3, "b", "c")

      {:ok, pl} = pl |> add_event(:new, :ask, 3, 60, 15)
      assert [{30, 10}, {50, 15}, {60, 15}] = PriceLevels.items(pl)
    end

    test "fill_up_until" do
      pl = PriceLevels.new(:ask)

      {:ok, pl} = pl |> add_event(:new, :ask, 1, 10, 5)
      assert [{10, 5}] = PriceLevels.items(pl)

      {:ok, pl} = pl |> PriceLevels.fill_up_until(3)
      assert [{10, 5}, {0, 0}, {0, 0}] = PriceLevels.items(pl)
    end

    test "small check" do
      pl = PriceLevels.new(:bid)
      {:ok, pl} = pl |> PriceLevels.handle_event(event_for(:new, :bid, 1, 50, 30))
      assert [{50, 30}] = PriceLevels.items(pl)

      {:ok, pl} = pl |> PriceLevels.handle_event(event_for(:new, :bid, 2, 40, 40))
      assert [{50, 30}, {40, 40}] = PriceLevels.items(pl)
    end

    def event_for(instruction, side, index, price, qty) do
      %Event{
        instruction: instruction,
        side: side,
        price_level_index: index,
        price: price,
        quantity: qty
      }
    end

    def add_event(pl, instruction, side, index, price, qty) do
      pl |> PriceLevels.handle_event(event_for(instruction, side, index, price, qty))
    end
  end
end
