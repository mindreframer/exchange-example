defmodule ExchangeTest do
  use ExUnit.Case

  test "conforms to commonly expected behaviour for exchanges" do
    {:ok, pid} = Exchange.start_link()
    SharedTestSuite.common_tests(Exchange ,pid)
  end
end
