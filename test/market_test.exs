defmodule Exchange.MarketTest do
  alias Exchange.{Fixtures, Market, Event, Util}
  use ExUnit.Case
  doctest Exchange.Market

  describe "feed" do
    test "adds a list of items to the market" do
      {:ok, market} = Util.feed_into_market(Market.new(), Fixtures.all())
      assert Enum.count(market.events) == 7

      {:ok, market} = Util.feed_into_market(Market.new(), [])
      assert Enum.count(market.events) == 0

      {:error, msg} = Util.feed_into_market(Market.new(), [%Event{}])
      assert msg == "invalid event %Exchange.Event{instruction: nil, price: nil, price_level_index: nil, quantity: nil, side: nil}"
    end
  end
end
