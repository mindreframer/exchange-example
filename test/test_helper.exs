ExUnit.start()

defmodule SharedTestSuite do
  @moduledoc """
  used to assert common behaviour for different exchange implementations
  """
  use ExUnit.Case

  def event_for(instruction, side, index, price, qty) do
    %{
      instruction: instruction,
      side: side,
      price_level_index: index,
      price: price,
      quantity: qty
    }
  end

  def assert_orders(mod, pid, level, orders) do
    assert mod.order_book(pid, level) == orders
  end

  def common_tests(mod, pid) do
    mod.send_instruction(pid, event_for(:new, :bid, 1, 50, 30))

    assert_orders(mod, pid, 1, [%{ask_price: 0, ask_quantity: 0, bid_price: 50, bid_quantity: 30}])

    mod.send_instruction(pid, event_for(:new, :bid, 2, 40, 40))

    assert_orders(mod, pid, 2, [
      %{ask_price: 0, ask_quantity: 0, bid_price: 50, bid_quantity: 30},
      %{ask_price: 0, ask_quantity: 0, bid_price: 40, bid_quantity: 40}
    ])

    mod.send_instruction(pid, event_for(:new, :ask, 1, 60, 10))

    assert_orders(mod, pid, 2, [
      %{ask_price: 60, ask_quantity: 10, bid_price: 50, bid_quantity: 30},
      %{ask_price: 0, ask_quantity: 0, bid_price: 40, bid_quantity: 40}
    ])

    mod.send_instruction(pid, event_for(:new, :ask, 2, 70, 10))

    assert_orders(mod, pid, 2, [
      %{ask_price: 60, ask_quantity: 10, bid_price: 50, bid_quantity: 30},
      %{ask_price: 70, ask_quantity: 10, bid_price: 40, bid_quantity: 40}
    ])

    mod.send_instruction(pid, event_for(:update, :ask, 2, 70, 20))

    assert_orders(mod, pid, 2, [
      %{ask_price: 60, ask_quantity: 10, bid_price: 50, bid_quantity: 30},
      %{ask_price: 70, ask_quantity: 20, bid_price: 40, bid_quantity: 40}
    ])

    mod.send_instruction(pid, event_for(:update, :bid, 1, 50, 40))

    assert_orders(mod, pid, 2, [
      %{ask_price: 60, ask_quantity: 10, bid_price: 50, bid_quantity: 40},
      %{ask_price: 70, ask_quantity: 20, bid_price: 40, bid_quantity: 40}
    ])

    mod.send_instruction(pid, event_for(:delete, :bid, 1, 0, 0))

    assert_orders(mod, pid, 2, [
      %{ask_price: 60, ask_quantity: 10, bid_price: 40, bid_quantity: 40},
      %{ask_price: 70, ask_quantity: 20, bid_price: 0, bid_quantity: 0}
    ])

    mod.send_instruction(pid, event_for(:delete, :ask, 2, 0, 0))

    assert_orders(mod, pid, 2, [
      %{ask_price: 60, ask_quantity: 10, bid_price: 40, bid_quantity: 40},
      %{ask_price: 0, ask_quantity: 0, bid_price: 0, bid_quantity: 0}
    ])

    assert_orders(mod, pid, 3, [
      %{ask_price: 60, ask_quantity: 10, bid_price: 40, bid_quantity: 40},
      %{ask_price: 0, ask_quantity: 0, bid_price: 0, bid_quantity: 0},
      %{ask_price: 0, ask_quantity: 0, bid_price: 0, bid_quantity: 0}
    ])

    assert {:error, "bad input: price_level_index -5"} == mod.order_book(pid, -5)
  end
end
