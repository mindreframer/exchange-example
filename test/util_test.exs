defmodule Exchange.UtilTest do
  use ExUnit.Case
  alias Exchange.Util

  describe "Util.fillup_list" do
    test "list size equals count" do
      assert Util.fillup_list([], 0, 100) == []
      assert Util.fillup_list([1], 1, 100) == [1]
      assert Util.fillup_list([1, 2], 2, 100) == [1, 2]
    end

    test "list size bigger than count" do
      assert Util.fillup_list([1, 2], 1, 100) == [1]
      assert Util.fillup_list([1, 2, 3], 2, 100) == [1, 2]
      assert Util.fillup_list([1, 2, 3, 4, 5], 2, 100) == [1, 2]
    end

    test "list size less than count" do
      assert Util.fillup_list([1, 2], 3, 100) == [1, 2, 100]
      assert Util.fillup_list([1, 2], 4, 100) == [1, 2, 100, 100]
      assert Util.fillup_list([1, 2], 5, 100) == [1, 2, 100, 100, 100]
    end
  end
end
