defmodule ExchangeDurableTest do
  use ExUnit.Case

  setup do
    File.rm_rf("test/some/db")
    :ok
  end

  test "conforms to commonly expected behaviour for exchanges" do
    {:ok, pid} = ExchangeDurable.start_link("test/some/db")
    SharedTestSuite.common_tests(ExchangeDurable, pid)
  end
end
