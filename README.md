# Exchange

Very simple Exchange implementation that covers 2 approaches:
  - purely ephemeral (`Exchange`)
  - with durability  (`ExchangeDurable`)

Test cases are shared for both implementations and can be found in `test/test_helper.exs`.

Durability is implemented with the help of [CubDB](https://github.com/lucaong/cubdb) to keep things as straightforward as possible.
Also the choice was made to keep things slightly `un-DRY` (2 Exchange modules) for better code clarity.



## CODE CHALLENGE

## Scope

The goal of this coding challenge is to allow you to demonstrate your programming skills while working with common data structures and algorithms. We highly value simple designs and elegant code that is easy to read. We encourage that you also focus on these qualities in your implementation.

We think your time is important, so we won’t take too much of it. We’d like you to spend a **maximum of four hours** on this. We care about a good balance between speed and quality, but it’s up to you to decide how that looks for you.

## The problem

The challenge is to simulate a simplified model of an order book of a financial exchange ([https://en.wikipedia.org/wiki/Order_book_(trading)](https://en.wikipedia.org/wiki/Order_book_(trading)))

You must write your solution as an Elixir program that can be executed from the Elixir's interactive shell (iex)

Your program must take as input events representing order book updates in chronological order, and produce, as output, the most recent order book ordered by price level.

The **input** is given to your program as events of the following type:

```elixir
%{
  instruction: :new | :update | :delete,
  side: :bid | :ask,
  price_level_index: interger()
  price: float()
  quantity: integer()
}
```

Your program must provide a function to feet events to the stock exchange. This function should have the following type:

```elixir
@spec send_instruction(
  exchange_pid: pid(),
  event: map()
) :: :ok | {:error, any()}
```

The **output** must be a list of book entries, one entry for each price level (1...book_depth). Where each entry matches the type:

```elixir
%{
  bid_price: float(),
  bid_quantity: integer(),
  ask_price: float()
  ask_quantity: integer()
}
```

Your program must have an entry point function with the following type

```elixir
@spec order_book(
  exchange: pid(),
  book_depth: interger()
) :: list(map())
```

Finally we need a start function to bootstrap the exchange and we could start sending events. We suggest we you go with the conventional `start_link`

**Market Update Instruction Types**

The semantics of the instruction types in the input events are the following:

- `:new` → Insert new price level. Existing price levels with a greater or equal index are shifted up
- `:delete` → Delete a price level. Existing price levels with a higher index will be shifted down
- `:update` → This event will contain new values to update an existing price level. If the event is for a price level that has not yet been created an error must be returned

**Book level**

Your program must keep track of all price levels received, however when producing the order_book we will only considering price levels that are less than or equal than the specified `book_depth`. Price level that have not been provided should have values of zero.

## Example

```elixir
iex(1)> {:ok, exchange_pid} = Exchange.start_link()
{:ok, #PID<0.141.0>}
iex(2)> Exchange.send_instruction(exchange_pid, %{
... instruction: :new,
... side: :bid,
... price_level_index: 1,
... price: 50.0,
... quantity: 30
... })
:ok
iex(3)> Exchange.send_instruction(exchange_pid, %{
... instruction: :new,
... side: :bid,
... price_level_index: 2,
... price: 40.0,
... quantity: 40
... })
:ok
iex(4)> Exchange.send_instruction(exchange_pid, %{
... instruction: :new,
... side: :ask,
... price_level_index: 1,
... price: 60.0,
... quantity: 10
... })
:ok
iex(5)> Exchange.send_instruction(exchange_pid, %{
... instruction: :new,
... side: :ask,
... price_level_index: 2,
... price: 70.0,
... quantity: 10
... })
:ok
iex(6)> Exchange.send_instruction(exchange_pid, %{
... instruction: :update,
... side: :ask,
... price_level_index: 2,
... price: 70.0,
... quantity: 20
... })
:ok
iex(7)> Exchange.send_instruction(exchange_pid, %{
... instruction: :update,
... side: :bid,
... price_level_index: 1,
... price: 50.0,
... quantity: 40
... })
:ok
iex(8)> Exchange.order_book(exchange_pid, 2)
[
  %{ask_price: 60.0, ask_quantity: 10, bid_price: 50.0, bid_quantity: 40},
  %{ask_price: 70.0, ask_quantity: 20, bid_price: 40.0, bid_quantity: 40}
]
```

## Extra

If you feel this is too easy and want to go an extra mile 😎, we welcome you to do so! One suggestion would be to add persistence to your process.

## Final Notes

Write down any assumptions you need to make. You’re welcome to reach out to us for any clarification necessary.

So please use this exercise to represent yourself in the best way possible. Have fun with it, and good luck!
