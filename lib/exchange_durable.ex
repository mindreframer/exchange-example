defmodule ExchangeDurable do
  @moduledoc """
  Top-level Genserver for an exchange with persistence, most of the logic is handled by the Market module.
  Durability is implemented in a very simplistic manner, we store events and replay them when loading from file.

  CubDB is used to store Elixir terms to disk as `{nanosecond, event}` tuples.
  Loading scans all records chronologically and applies them to the in-memory state of the market.

  Pros:
    - very simple and straightforward implementation

  Cons:
    - potentially long initialization time, yet this is handled in background after the process has started

  Potential improvements:
    - make the persistence mechanism abstract by using `Behaviour` and implement an adapter for CubDB
    - keep an in-memory buffer that flushes data to CubDB periodically for better throughput (and reduced guarantees)
    - slim down the event datastructure to be more compact (tuples/records instead of structs)
    - handle data loading in a streaming fashing, dont try to eagerly load all events in memory

  Those improvements might complicate the initial solution quite heavily and it should be considered carefully,
  if they are really required for a particular usecase.

  """
  alias Exchange.Market
  use GenServer

  def start_link(file) when is_binary(file) do
    GenServer.start_link(__MODULE__, file)
  end

  def init(file) do
    state = %{
      file: file,
      market: Market.new(),
      db: nil
    }

    {:ok, state, {:continue, :init_from_db}}
  end

  def handle_continue(:init_from_db, %{file: file, market: market}) do
    with {:ok, db} <- CubDB.start_link(file),
         {:ok, events} <- CubDB.select(db, min_key: 1),
         events <- events |> Enum.map(&take_value/1),
         {:ok, market} <- Exchange.Util.feed_into_market(market, events) do
      {:noreply, as_state(market, db)}
    end
  end

  @spec send_instruction(exchange :: pid(), event :: map()) :: {:ok} | {:error, reason: any()}
  def send_instruction(exchange, event) do
    GenServer.call(exchange, {:send_instruction, event})
  end

  @spec order_book(exchange :: pid(), book_depth :: integer()) :: list(map())
  def order_book(pid, price_level) do
    GenServer.call(pid, {:order_book, price_level})
  end

  def handle_call({:send_instruction, event}, _from, %{market: market, db: db}) do
    with {:ok, market} <- Market.add(market, event),
         :ok <- CubDB.put(db, System.system_time(:nanosecond), event) do
      {:reply, :ok, as_state(market, db)}
    end
  end

  def handle_call({:order_book, price_level_index}, _from, %{market: market, db: db}) do
    {:reply, Market.order_book(market, price_level_index), as_state(market, db)}
  end

  defp take_value({_k, v}), do: v
  def as_state(market, db), do: %{market: market, db: db}
end
