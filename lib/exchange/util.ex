defmodule Exchange.Util do
  @moduledoc """
  place for random functions
  """
  alias Exchange.Market

  @doc """
  adds a list of events to a market
  """
  def feed_into_market(market, list) do
    list
    |> Enum.reduce(market, fn item, market ->
      with {:ok, market} <- Market.add(market, item) do
        market
      end
    end)
    |> wrap()
  end

  # a little workaround to always have either {:ok, res} or {:error, msg} tuples
  defp wrap({:error, m}), do: {:error, m}
  defp wrap(m), do: {:ok, m}

  @doc """
  fills a list up to a given size with the provided element
  """
  def fillup_list(list, count, _el) when length(list) > count do
    list |> Enum.take(count)
  end

  def fillup_list(list, count, _el) when length(list) == count do
    list
  end

  def fillup_list(list, count, el) when length(list) < count do
    missing_parts = Stream.cycle([el]) |> Stream.take(count - length(list)) |> Enum.to_list()
    input_list = list |> Enum.reverse()
    Enum.reduce(missing_parts, input_list, fn x, acc -> [x | acc] end) |> Enum.reverse()
  end
end
