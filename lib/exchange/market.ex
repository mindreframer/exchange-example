defmodule Exchange.Market do
  @moduledoc """
  Pure logic to manipulate a struct representing a market data
  """
  alias Exchange.{Market, Event, PriceLevels}
  defstruct events: [], ask: PriceLevels.new(:ask), bid: PriceLevels.new(:bid)

  @valid_instructions [:new, :update, :delete]

  @type t :: %Market{
          events: [Event.t()]
        }

  def new() do
    %Exchange.Market{events: []}
  end

  @doc """
  adds new events to a Market struct
  """
  @spec add(market :: Market.t(), event :: map() | Event.t()) :: {:ok} | {:error, reason: any()}
  def add(
        %Market{events: events, bid: bid, ask: ask},
        event = %Event{instruction: instruction}
      )
      when instruction in @valid_instructions do
    with {:ok, ask} <- ask |> PriceLevels.handle_event(event),
         {:ok, bid} <- bid |> PriceLevels.handle_event(event) do
      {:ok, %Market{events: [event | events], ask: ask, bid: bid}}
    end
  end

  def add(market, event = %{instruction: instruction})
      when instruction in @valid_instructions do
    with {:ok, e} <- Event.new(event) do
      add(market, e)
    end
  end

  def add(_, event) do
    {:error, "invalid event #{inspect(event)}"}
  end


  @spec order_book(market :: Market.t(), price_level_index :: integer()) :: list(map())
  def order_book(%Market{bid: bid, ask: ask}, price_level_index)
      when is_integer(price_level_index) and price_level_index > 0 do
    {:ok, bid} = PriceLevels.fill_up_until(bid, price_level_index)
    {:ok, ask} = PriceLevels.fill_up_until(ask, price_level_index)

    Enum.zip(bid.items, ask.items) |> Enum.map(&make_ask_bid_map/1)
  end

  def order_book(%Market{}, price_level_index) do
    {:error, "bad input: price_level_index #{inspect price_level_index}"}
  end

  defp make_ask_bid_map({{bid_price, bid_quantity}, {ask_price, ask_quantity}}) do
    %{
      ask_price: ask_price,
      ask_quantity: ask_quantity,
      bid_price: bid_price,
      bid_quantity: bid_quantity
    }
  end
end
