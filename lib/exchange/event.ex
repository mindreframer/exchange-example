defmodule Exchange.Event do
  alias __MODULE__

  defstruct instruction: nil, side: nil, price_level_index: nil, price: nil, quantity: nil

  @type t :: %Event{
          instruction: :new | :update | :delete,
          side: :bid | :ask,
          price_level_index: integer(),
          price: float(),
          quantity: integer()
        }

  @spec new(event: map()) :: {:ok, Event.t()} | {:error, reason: any()}
  def new(event) do
    {:ok,
     %Event{
       instruction: event.instruction,
       side: event.side,
       price_level_index: event.price_level_index,
       price: event.price,
       quantity: event.quantity
     }}
  end
end
