defmodule Exchange.Fixtures do
  @moduledoc """
  Quick sample data to play around in IEx
  """
  alias Exchange.Event
  def get(idx) do
    all() |> Enum.at(idx)
  end

  def index(idx) do
    all() |> Enum.filter(fn(x)-> x.price_level_index == idx end)
  end

  def all do
    [
      event_for(:new, :bid, 1, 50, 30),
      event_for(:new, :bid, 2, 40, 40),
      event_for(:new, :ask, 1, 60, 10),
      event_for(:new, :ask, 2, 70, 10),
      event_for(:update, :ask, 2, 70, 20),
      event_for(:update, :bid, 1, 50, 40),
      event_for(:new, :bid, 3, 65, 35)
    ]
  end

  defp event_for(instruction, side, index, price, qty) do
    %Event{
      instruction: instruction,
      side: side,
      price_level_index: index,
      price: price,
      quantity: qty
    }
  end
end
