defmodule Exchange.PriceLevels do
  @moduledoc """
  Implements book-keeping for price level operations.
  I used a plain list because element shifting can be modelled very efficiently.

  Assumptions:
    - insertion of a new price level is only possible when at least the index before exists, eg: insert index 2 only when index 1 present
    - to keep public API simple filtering for the correct event side (bid / ask) happens in
      the `handle_event` function, with a noop for non-matching events
    -
  """
  alias Exchange.{PriceLevels, Event}

  defstruct type: nil, items: []

  def new(type) when type in [:ask, :bid] do
    %PriceLevels{type: type, items: []}
  end

  def handle_event(price_levels = %PriceLevels{type: type}, %Event{
        side: side,
        price: price,
        quantity: quantity,
        instruction: instruction,
        price_level_index: price_level_index
      })
      when type == side do
    case instruction do
      :new -> insert(price_levels, price_level_index, {price, quantity})
      :update -> update(price_levels, price_level_index, {price, quantity})
      :delete -> delete(price_levels, price_level_index)
    end
  end

  def handle_event(price_levels, _event) do
    # noop
    {:ok, price_levels}
  end

  def fill_up_until(price_levels = %PriceLevels{items: items}, index) do
    items = items |> Exchange.Util.fillup_list(index, {0, 0})
    {:ok, %PriceLevels{price_levels | items: items}}
  end

  def items(price_levels) do
    price_levels.items
  end

  defp insert(price_levels = %PriceLevels{items: items}, index, item)
      when length(items) >= index - 1 do
    with {:ok} <- validate(item) do
      items = List.insert_at(items, index - 1, item)
      {:ok, %PriceLevels{price_levels | items: items}}
    end
  end

  defp insert(_price_levels, level, _item) do
    outofboundserror(level)
  end

  defp update(price_levels = %PriceLevels{items: items}, index, item)
      when length(items) >= index do
    with {:ok} <- validate(item) do
      items = List.replace_at(items, index - 1, item)
      {:ok, %PriceLevels{price_levels | items: items}}
    end
  end

  defp update(_price_levels, level, _item) do
    outofboundserror(level)
  end

  defp delete(price_levels = %PriceLevels{items: items}, index) when length(items) >= index do
    items = List.delete_at(items, index - 1)
    {:ok, %PriceLevels{price_levels | items: items}}
  end

  defp delete(_price_levels, index) do
    outofboundserror(index)
  end

  defp validate(item) do
    cond do
      not (is_tuple(item) and is_number(elem(item, 0)) and is_integer(elem(item, 1))) ->
        {:error, "bad item format: #{inspect(item)}"}

      true ->
        {:ok}
    end
  end

  defp outofboundserror(index) do
    {:error, "outofbounds: index: #{index}"}
  end
end
