defmodule Exchange do
  @moduledoc """
  Top-level Genserver for an exchange, most of the logic is handled by the Market module
  """
  alias Exchange.Market
  use GenServer

  def start_link(market \\ Market.new()) do
    GenServer.start_link(__MODULE__, market)
  end

  def init(market = %Market{}) do
    {:ok, market}
  end

  @spec send_instruction( exchange :: pid(), event :: map()) :: {:ok} | {:error, reason: any()}
  def send_instruction(exchange, event) do
    GenServer.call(exchange, {:send_instruction, event})
  end

  @spec order_book( exchange :: pid(), book_depth :: integer()) :: list(map())
  def order_book(pid, price_level) do
    GenServer.call(pid, {:order_book, price_level})
  end

  def handle_call({:send_instruction, event}, _from, market = %Market{}) do
    with {:ok, market} <- Market.add(market, event) do
      {:reply, :ok, market}
    end
  end

  def handle_call({:order_book, price_level_index}, _from, market = %Market{}) do
    {:reply, Market.order_book(market, price_level_index), market}
  end
end
